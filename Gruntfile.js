module.exports = function (grunt) {
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.initConfig({
        concat:{
            dist: {
                src: ['src/spiff-service.module.js','src/*.js'],
                dest: 'dist/spiff-service-angularjs-sdk.js'
            }
        }
    });
    grunt.registerTask('default', ['concat']);
};