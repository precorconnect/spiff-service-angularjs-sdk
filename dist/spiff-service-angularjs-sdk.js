(function () {
    angular.module(
        "spiffServiceModule",
        []);
})();
(function () {
    angular
        .module("spiffServiceModule")
        .provider(
        'spiffServiceConfig',
        spiffServiceConfigProvider
    );

    function spiffServiceConfigProvider() {

        var objectUnderConstruction = {
            setBaseUrl: setBaseUrl,
            $get: $get
        };

        return objectUnderConstruction;

        function setBaseUrl(baseUrl) {
            objectUnderConstruction.baseUrl = baseUrl;
            return objectUnderConstruction;
        }

        function $get() {
            return {
                baseUrl: objectUnderConstruction.baseUrl
            }
        }
    }
})();
(function () {
    angular
        .module('spiffServiceModule')
        .factory(
        'spiffServiceClient',
        [
            'spiffServiceConfig',
            '$http',
            '$q',
            spiffServiceClient
        ]);

    function spiffServiceClient(spiffServiceConfig,
                                $http,
                                $q) {

        var thisFactory = this;
        thisFactory.cachedSpiffs = null;
        thisFactory.lastAddedSpiff = null;

        return {
            addSpiff: addSpiff,
            constructAddSpiffRequest: constructAddSpiffRequest,
            constructDeactivateSpiffRequest: constructDeactivateSpiffRequest,
            constructSpiffCompositeProductRequirement: constructSpiffCompositeProductRequirement,
            constructSpiffSimpleProductRequirement: constructSpiffSimpleProductRequirement,
            deactivateSpiff: deactivateSpiff,
            getSpiffWithId: getSpiffWithId,
            getSpiffClaimWithId: getSpiffClaimWithId,
            listCurrentPartnerSpiffClaims: listCurrentPartnerSpiffClaims,
            listCurrentPartnerSpiffEntitlements: listCurrentPartnerSpiffEntitlements,
            listSpiffs: listSpiffs,
            submitSpiffClaim: submitSpiffClaim
        };

        /**
         * @typedef {Object} AddSpiffRequest
         * @property {number} amount
         * @property {number} validFromTimestamp
         * @property {number} validToTimestamp
         * @property {SpiffProductRequirement} productRequirement
         */

        /**
         * Adds a spiff
         * @param {AddSpiffRequest} addSpiffRequest
         * @returns a promise of {string} the id of the spiff
         */
        function addSpiff(addSpiffRequest) {

            var request = $http({
                method: "post",
                url: spiffServiceConfig.baseUrl + "/spiffs",
                data: addSpiffRequest
            });

            return request
                .then(
                function (spiffId) {

                    // @TODO: need to get created spiff from service
                    var spiff = addSpiffRequest;
                    spiff.Id = spiffId;

                    // add to cached spiffs if populated
                    if (thisFactory.cachedSpiffs) {
                        thisFactory.cachedSpiffs.push(spiff)
                    }

                    // set last added spiff
                    thisFactory.lastAddedSpiff = spiff;

                    return spiffId;

                });

        }

        /**
         * A spiff deactivation
         * @typedef {Object} SpiffDeactivation
         * @property {string} reason
         * @property {number} timestamp
         */

        /**
         * A sales incentive
         * @typedef {Object} Spiff
         * @property {number} amount - the $ amount
         * @property {SpiffDeactivation} deactivation
         * @property {number} validFromTimestamp - the unix creationTimestamp at which the spiff becomes valid
         * @property {number} validToTimestamp - the unix creationTimestamp at which the spiff ceases to be valid
         * @property {(SpiffSimpleProductRequirement|SpiffCompositeProductRequirement)} productRequirement
         * @property {string} id
         * @property {number} creationTimestamp - the unix creationTimestamp at which the spiff was created
         */

        /**
         * Constructs an AddSpiffRequest
         * @returns {AddSpiffRequest}
         */
        function constructAddSpiffRequest() {
            return {
                amount: null,
                validFromTimestamp: null,
                validToTimestamp: null,
                productRequirement: null
            }
        }

        /**
         * @typedef {Object} SpiffProductRequirement
         * @property {string} type - either "simple" or "composite"
         */

        /**
         * Spiff composite product requirement
         * @typedef {Object} SpiffCompositeProductRequirement
         * @augments {SpiffProductRequirement}
         * @property {SpiffSimpleProductRequirement[]} components
         */

        /**
         * Constructs a SpiffCompositeProductRequirement
         * @returns {SpiffCompositeProductRequirement}
         */
        function constructSpiffCompositeProductRequirement() {
            return {
                type: "composite",
                components: []
            }
        }

        /**
         * Spiff simple product requirement
         * @typedef {Object} SpiffSimpleProductRequirement
         * @augments {SpiffProductRequirement}
         * @property {string} productLine
         * @property {string} productGroup
         */

        /**
         * Constructs a SpiffSimpleProductRequirement
         * @returns {SpiffSimpleProductRequirement}
         */
        function constructSpiffSimpleProductRequirement() {
            return {
                type: "simple",
                productLine: null,
                productGroup: null
            }
        }

        /**
         * @typedef {Object} DeactivateSpiffRequest
         * @property {number} spiffId
         * @property {string} reason
         */

        /**
         * Constructs a DeactivateSpiffRequest
         * @returns {DeactivateSpiffRequest}
         */
        function constructDeactivateSpiffRequest() {
            return {
                spiffId: null,
                reason: null
            }
        }

        /**
         * Deactivates a Spiff
         * @param {DeactivateSpiffRequest} deactivateSpiffRequest
         */
        function deactivateSpiff(deactivateSpiffRequest) {

            return $http({
                method: "post",
                url: spiffServiceConfig.baseUrl + "/spiffs/" + deactivateSpiffRequest.spiffId + "/deactivation",
                data: {reason: deactivateSpiffRequest.reason}
            });

        }

        /**
         * Gets the spiff with the provided id
         * @param {number} id
         * @returns a promise of {Spiff}
         */
        function getSpiffWithId(id) {
            var deferred = $q.defer();
            if (thisFactory.lastAddedSpiff.id == id) {
                deferred.resolve(
                    thisFactory.lastAddedSpiff
                );
            }
            else {
                // @TODO: fetch from service
                deferred.resolve(
                    {
                        "amount": 1000,
                        "validFromTimestamp": 1435039986,
                        "validToTimestamp": 1435039986,
                        "productRequirement": {
                            "type": "simple",
                            "productLine": "Treadmill Base - Commercial",
                            "productGroup": "Treadmill - Commercial"
                        },
                        "id": id,
                        "creationTimestamp": 1435039986,
                        "deactivation": null
                    }
                )
            }
            return deferred.promise;
        }

        /**
         * @typedef {Object} SpiffSynopsis
         * @property {string} id
         * @property {number} amount
         */

        /**
         * @typedef {Object} RepSynopsis
         * @property {string} id
         * @property {string} firstName
         * @property {string} lastName
         */

        /**
         * @typedef {Object} ConsumerSynopsis
         * @property {string} firstName
         * @property {string} lastName
         */

        /**
         * @typedef {Object} FacilitySynopsis
         * @property {string} name
         */

        /**
         * @typedef {Object} InvoiceSynopsis
         * @property {string} url
         * @property {string} number
         */

        /**
         * @typedef {Object} PartnerSaleSynopsis
         * @property {InvoiceSynopsis} invoice
         * @property {RepSynopsis} rep
         * @property {string} registrationId
         * @property {number} creationTimestamp - the instant the sale was made
         */

        /**
         * A brief summary of a partners consumer sale
         * @typedef {Object} PartnerConsumerSaleSynopsis
         * @augments PartnerSaleSynopsis
         * @property {ConsumerSynopsis} consumer
         */

        /**
         * A brief summary of a partners commercial sale
         * @typedef {Object} PartnerCommercialSaleSynopsis
         * @augments PartnerSaleSynopsis
         * @property {FacilitySynopsis} facility
         */

        /**
         * Spiff information for a single sold product
         * @typedef {Object} LineItem
         * @property {SpiffSynopsis[]} spiffs
         */

        /**
         * Spiff information for a single simple product
         * @typedef {Object} SimpleLineItem
         * @augments LineItem
         * @property {string} serialNumber
         * @property {string} productLine
         */

        /**
         * Spiff information for a single composite product
         * @typedef {Object} CompositeLineItem
         * @augments LineItem
         * @property {SimpleLineItem[]} components
         */

        /**
         * A summary of a single spiff claim
         * @typedef {Object} SpiffClaimSummary
         * @property {SpiffEntitlementSummary[]} entitlements - spiff entitlements in this claim
         * @property {RepSynopsis} rep
         * @property {string} id
         * @property {number} creationTimestamp
         */

        /**
         * Gets the spiff claim with the provided id
         * @param id
         * @returns a promise of {SpiffClaimSummary}
         */
        function getSpiffClaimWithId(id) {
            // use dummy data for now
            var deferred = $q.defer();
            deferred.resolve(
                {
                    entitlements: [
                        {
                            sale: {
                                facility: {
                                    name: "LA Fitness"
                                },
                                invoice: {
                                    number: "234333333",
                                    url: "http://www.precor.com/en-us"
                                },
                                rep: {
                                    id: "23423383",
                                    firstName: "Sally",
                                    lastName: "Repworth"
                                },
                                registrationId: "2343232AAd",
                                registrationTimestamp: 1333297471,
                                creationTimestamp: 1333297471
                            },
                            lineItems: [
                                {
                                    productLine: "Treadmill Display - Commercial",
                                    serialNumber: "2333ADSSDD2",
                                    spiffs: [
                                        {
                                            id: "21433222",
                                            amount: 33.00
                                        }
                                    ]
                                },
                                {
                                    components: [
                                        {
                                            productLine: "Treadmill Display - Commercial",
                                            serialNumber: "2333ADSSDD2",
                                            spiffs: [
                                                {
                                                    id: "23433272",
                                                    amount: 25.00
                                                }
                                            ]
                                        },
                                        {
                                            productLine: "Treadmill Base - Commercial",
                                            serialNumber: "AADSDFDS233233",
                                            spiffs: [
                                                {
                                                    id: "63433272",
                                                    amount: 30.00
                                                }
                                            ]
                                        }
                                    ],
                                    spiffs: [
                                        {
                                            id: "23433228",
                                            amount: 15.00
                                        },
                                        {
                                            id: "23437222",
                                            amount: 10.00
                                        }
                                    ]
                                }
                            ],
                            total: 113,
                            id: "saads8d8d8822"
                        }
                    ],
                    rep: {
                        id: "23423383",
                        firstName: "Sally",
                        lastName: "Repworth"
                    },
                    id: "23433333",
                    creationTimestamp: 1393297471
                }
            );

            return deferred.promise;
        }

        /**
         * Lists the current partners spiff claims in descending sale registrationTimestamp order
         * @param skip
         * @param take
         * @returns a promise of {SpiffClaimSummary[]}
         */
        function listCurrentPartnerSpiffClaims(skip, take) {
            // use dummy data for now
            var deferred = $q.defer();
            deferred.resolve([
                {
                    entitlements: [
                        {
                            sale: {
                                facility: {
                                    name: "LA Fitness"
                                },
                                invoice: {
                                    number: "234333333",
                                    url: "http://www.precor.com/en-us"
                                },
                                rep: {
                                    id: "23423383",
                                    firstName: "Sally",
                                    lastName: "Repworth"
                                },
                                registrationId: "2343232AAd",
                                registrationTimestamp: 1333297471,
                                creationTimestamp: 1333297471
                            },
                            lineItems: [
                                {
                                    productLine: "Treadmill Display - Commercial",
                                    serialNumber: "2333ADSSDD2",
                                    spiffs: [
                                        {
                                            id: "21433222",
                                            amount: 33.00
                                        }
                                    ]
                                },
                                {
                                    components: [
                                        {
                                            productLine: "Treadmill Display - Commercial",
                                            serialNumber: "2333ADSSDD2",
                                            spiffs: [
                                                {
                                                    id: "23433272",
                                                    amount: 25.00
                                                }
                                            ]
                                        },
                                        {
                                            productLine: "Treadmill Base - Commercial",
                                            serialNumber: "AADSDFDS233233",
                                            spiffs: [
                                                {
                                                    id: "63433272",
                                                    amount: 30.00
                                                }
                                            ]
                                        }
                                    ],
                                    spiffs: [
                                        {
                                            id: "23433228",
                                            amount: 15.00
                                        }
                                    ]
                                }
                            ],
                            total: 113,
                            id: "saads8d8d8822"
                        },
                        {
                            sale: {
                                facility: {
                                    name: "LA Fitness"
                                },
                                invoice: {
                                    number: "234333333",
                                    url: "http://www.precor.com/en-us"
                                },
                                rep: {
                                    id: "23423383",
                                    firstName: "Sally",
                                    lastName: "Repworth"
                                },
                                registrationId: "2343232AAd",
                                registrationTimestamp: 1333297471,
                                creationTimestamp: 1333297471
                            },
                            lineItems: [
                                {
                                    productLine: "Treadmill Display - Commercial",
                                    serialNumber: "2333ADSSDD2",
                                    spiffs: [
                                        {
                                            id: "21433222",
                                            amount: 33.00
                                        }
                                    ]
                                },
                                {
                                    components: [
                                        {
                                            productLine: "Treadmill Display - Commercial",
                                            serialNumber: "2333ADSSDD2",
                                            spiffs: [
                                                {
                                                    id: "23433272",
                                                    amount: 25.00
                                                }
                                            ]
                                        },
                                        {
                                            productLine: "Treadmill Base - Commercial",
                                            serialNumber: "AADSDFDS233233",
                                            spiffs: [
                                                {
                                                    id: "63433272",
                                                    amount: 30.00
                                                }
                                            ]
                                        }
                                    ],
                                    spiffs: [
                                        {
                                            id: "23437222",
                                            amount: 10.00
                                        }
                                    ]
                                }
                            ],
                            total: 113,
                            id: "saads8d8d8822"
                        }
                    ],
                    rep: {
                        id: "23423383",
                        firstName: "Sally",
                        lastName: "Repworth"
                    },
                    id: "23433333",
                    creationTimestamp: 1393297471
                },
                {
                    entitlements: [
                        {
                            sale: {
                                facility: {
                                    name: "LA Fitness"
                                },
                                invoice: {
                                    number: "234333333",
                                    url: "http://www.precor.com/en-us"
                                },
                                rep: {
                                    id: "23423383",
                                    firstName: "Sally",
                                    lastName: "Repworth"
                                },
                                registrationId: "2343232AAd",
                                registrationTimestamp: 1333297471,
                                creationTimestamp: 1333297471
                            },
                            lineItems: [
                                {
                                    productLine: "Treadmill Display - Commercial",
                                    serialNumber: "2333ADSSDD2",
                                    spiffs: [
                                        {
                                            id: "21433222",
                                            amount: 33.00
                                        }
                                    ]
                                },
                                {
                                    components: [
                                        {
                                            productLine: "Treadmill Display - Commercial",
                                            serialNumber: "2333ADSSDD2",
                                            spiffs: [
                                                {
                                                    id: "23433272",
                                                    amount: 25.00
                                                }
                                            ]
                                        },
                                        {
                                            productLine: "Treadmill Base - Commercial",
                                            serialNumber: "AADSDFDS233233",
                                            spiffs: [
                                                {
                                                    id: "63433272",
                                                    amount: 30.00
                                                }
                                            ]
                                        }
                                    ],
                                    spiffs: [
                                        {
                                            id: "23433228",
                                            amount: 15.00
                                        }
                                    ]
                                }
                            ],
                            total: 113,
                            id: "saads8d8d8822"
                        }
                    ],
                    rep: {
                        id: "23423383",
                        firstName: "Sally",
                        lastName: "Repworth"
                    },
                    id: "23433333",
                    creationTimestamp: 1393297471
                }
            ]);

            return deferred.promise;
        }

        /**
         * A summary of a partners right to spiffs earned from a single sale
         * @typedef {Object} SpiffEntitlementSummary
         * @property {PartnerSaleSynopsis} sale
         * @property {LineItem[]} lineItems
         * @property {number} total
         * @property {string} id
         */

        /**
         * Lists the current partners spiff entitlements in descending creationTimestamp order
         * @param skip
         * @param take
         * @returns a promise of {SpiffEntitlementSummary[]}
         */
        function listCurrentPartnerSpiffEntitlements(skip, take) {
            // use dummy data for now
            var deferred = $q.defer();
            deferred.resolve([
                {
                    sale: {
                        consumer: {
                            firstName: "johnny",
                            lastName: "consumerman"
                        },
                        invoice: {
                            number: "234333333",
                            url: "http://www.precor.com/en-us"
                        },
                        rep: {
                            id: "23423383",
                            firstName: "John",
                            lastName: "Hahknee"
                        },
                        registrationId: "1343232AAd",
                        creationTimestamp: 1333297471
                    },
                    lineItems: [
                        {
                            productLine: "Treadmill Display - Commercial",
                            serialNumber: "2333ADSSDD2",
                            spiffs: [
                                {
                                    id: "21433222",
                                    amount: 33.00
                                }
                            ]
                        }
                    ],
                    total: 33,
                    id: "saads8d8d8822"
                },
                {
                    sale: {
                        consumer: {
                            firstName: "johnny",
                            lastName: "consumerman"
                        },
                        invoice: {
                            number: "234333333",
                            url: "http://www.precor.com/en-us"
                        },
                        rep: {
                            id: "23423383",
                            firstName: "Sally",
                            lastName: "Repworth"
                        },
                        registrationId: "9943232AAd",
                        creationTimestamp: 1333297471
                    },
                    lineItems: [
                        {
                            productLine: "Treadmill Display - Commercial",
                            serialNumber: "2333ADSSDD2",
                            spiffs: [
                                {
                                    id: "21433222",
                                    amount: 33.00
                                }
                            ]
                        }
                    ],
                    total: 33,
                    id: "saads8d8d8822"
                },
                {
                    sale: {
                        facility: {
                            name: "LA Fitness"
                        },
                        invoice: null,
                        rep: null,
                        registrationId: "uu43232AAd",
                        creationTimestamp: 1333297471
                    },
                    lineItems: [
                        {
                            productLine: "Treadmill Display - Commercial",
                            serialNumber: "2333ADSSDD2",
                            spiffs: [
                                {
                                    id: "21433222",
                                    amount: 33.00
                                }
                            ]
                        },
                        {
                            components: [
                                {
                                    productLine: "Treadmill Display - Commercial",
                                    serialNumber: "2333ADSSDD2",
                                    spiffs: [
                                        {
                                            id: "23433272",
                                            amount: 25.00
                                        }
                                    ]
                                },
                                {
                                    productLine: "Treadmill Base - Commercial",
                                    serialNumber: "AADSDFDS233233",
                                    spiffs: [
                                        {
                                            id: "63433272",
                                            amount: 30.00
                                        }
                                    ]
                                }
                            ],
                            spiffs: [
                                {
                                    id: "23433228",
                                    amount: 15.00
                                }
                            ]
                        }
                    ],
                    total: 113,
                    id: "saads8d8d8822"
                }
            ]);

            return deferred.promise;
        }

        /**
         * Lists spiffs in descending creationTimestamp order
         * @returns a promise of {Spiff[]}
         */
        function listSpiffs() {

            if (thisFactory.cachedSpiffs) {
                var deferred = $q.defer();
                deferred.resolve(thisFactory.cachedSpiffs);
                return deferred.promise;
            }
            else {
                return $http({
                    method: "get",
                    url: spiffServiceConfig.baseUrl + "/spiffs"
                })
                    .then(function (response) {
                        // cache
                        thisFactory.cachedSpiffs = response.data;
                        return thisFactory.cachedSpiffs;
                    });
            }

        }

        /**
         * Submits a spiff claim
         * @param {string[]} spiffEntitlementIds - ids of spiff entitlements being claimed
         * @returns a promise of {string} the id of the spiff claim
         */
        function submitSpiffClaim(spiffEntitlementIds) {
            // use dummy data for now
            var deferred = $q.defer();
            deferred.resolve((new Date().getTime()));
            return deferred.promise;
        }
    }
})();
