## Description
An AngularJS Module implementing common use cases encountered when integrating AngularJS apps
 with the precorconnect spiff service.

## UseCases

#####addSpiff
Adds a spiff

#####constructAddSpiffRequest
Constructs an AddSpiffRequest

#####constructDeactivateSpiffRequest
Constructs a DeactivateSpiffRequest

#####constructSpiffCompositeProductRequirement
Constructs a SpiffCompositeProductRequirement

#####constructSpiffSimpleProductRequirement
Constructs a SpiffSimpleProductRequirement

#####deactivateSpiff
Deactivates a Spiff

#####getSpiffWithId
Gets the spiff with the provided id

#####getSpiffClaimWithId
Gets the spiff claim with the provided id

#####listCurrentPartnerSpiffClaims
Lists the current partners spiff claims in descending creationTimestamp order

#####listCurrentPartnerSpiffEntitlements
Lists the current partners spiff entitlements in descending sale registrationTimestamp order

#####listSpiffs
Lists spiffs in descending createdTimestamp order

#####submitSpiffClaim
Submits a spiff claim

## Installation
add as bower dependency
    
```shell
bower install https://bitbucket.org/precorconnect/spiff-service-angularjs-sdk.git --save
```  
include in view  
```html
<script src="bower-components/angular/angular.js"></script>
<script src="bower-components/spiff-service-angularjs-sdk/dist/spiff-service-angularjs-sdk.js"></script>
```  
configure  
see below.

## Configuration 
####Properties
| Name (* denotes required) | Description |
|------|-------------|
| baseUrl* | The base url of the spiff service. |

#### Example
```js
angular.module(
        "app",
        ["spiffServiceModule"])
        .config(
        [
            "spiffServiceConfigProvider",
            appConfig
        ]);

    function appConfig(spiffServiceConfigProvider) {
        spiffServiceConfigProvider
            .setBaseUrl("@@spiffServiceBaseUrl");
    }
```